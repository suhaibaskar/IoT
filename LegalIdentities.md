Legal Identities
=====================

It is possible to assign a legal identity to an account. By assigning a legal identity to the account, it becomes possible for the account 
to sign legal contracts. Such contracts can be used by owners to regulate conditions for accessing their things, allowing for automation of 
decision support and provisioning.

| Legal Identities                                                      ||
| ------------|----------------------------------------------------------|
| Namespace:  | urn:ieee:iot:leg:id:1.0                                  |
| Schema:     | [LegalIdentities.xsd](Schemas/LegalIdentities.xsd)       |


Motivation and design goal
----------------------------

The method of manging legal identities described here, is designed with the following goals in mind:

* Legal Identities are protected using public key cryptography, where the client retains a private key that it does not share with anyone,
and registers the identity with a public key, that everyone with access to the identity receives. The private key is used by the client to
sign its legal identity application. The public key is used to validate the signature.

* Legal Identities are only available to their corresponding owners, parts in smart contracts, and operator (Trust Provider) staff,
acting as electronic notaries attesting to the validity of the legal identities.

* The broker (Trust Provider) maintains its own public and private key. Everyone has access to the public key. The private key is used by
the broker to sign that it attests to the validity of a claim, such as the integrity of a legal identity.

Getting Server Public Key
------------------------------

To get the public key of the server, the `getPublicKey` element is sent in an `iq get` stanza to the corresponding component.

Example:

```xml
<iq type='get' id='13' to='legal.example.org'>
	<getPublicKey xmlns="urn:ieee:iot:leg:id:1.0"/>
</iq>
```

The response contains the public key in a `publicKey` element, if successful. Public keys are encoded using the 
[End-to-End Encryption](E2E.md) namespace and elements.

Example:

```xml
<iq id='13' type='result' to='client@example.org/990d3f8fbb7f5a0bf39c7fdeda1ac9d7' from='legal.example.org'>
	<publicKey xmlns='urn:ieee:iot:leg:id:1.0'>
		<p521 
			x='AeQ/wjK0fiRwTQweGBzNEZhrmoYxGjMMCFQZqEkaeH1AXW/lNo/Ey0SyQ3lbYWR9vSN7B2krb11wRxfkBL1+ChHp' 
			y='rrPuhPEjE9JfNS8+HPoF0DTCoEqw2gGZg53sw0UlSa2/kICC5HxtLBpxG1XtVmonA8ZCb+sOgbajlWyzhjgDEC8=' 
			xmlns='urn:ieee:iot:e2e:1.0'/>
	</publicKey>
</iq>
```

Applying for Legal Identity registration
------------------------------------------

Legal identities are validated and attested by the broker out-of-band. To start the process, the client sends an application for a
legal identity to be registered by sending the `apply` element in an `iq set` stanza to the legal component of the server. The operator 
is notified, and validation can be performed, either manually, or automatically, depending on the context. How this process is done lies 
outside the scope of this specification.

The `apply` element must contain the information about the legal identity, encoded in an `identity` element. This element must not contain
an `id` attribute. Such requests must be rejected. The `id` attribute is added by the broker, after validating the request. The `identity`
element contains a sequence of child elements however. The first is a `clientPublicKey` element, which contains the public key of the client 
making the request. The corresponding private key will be used to sign the request later. Then comes a sequence of `property` elements. Each 
one encodes a `name`/`value` pair. It is up to the client to decide the number of properties included, and which ones. Any names can be used.
Some names are predefined however, as described in the following table:

| Property  | Description          |
|:----------|:---------------------|
| `FIRST`   | First name           |
| `MIDDLE`  | Middle name          |
| `LAST`    | Last name            |
| `PNR`     | Personal number      |
| `ADDR`    | Address              |
| `ADDR2`   | Address, second line |
| `ZIP`     | Zip or postal code   |
| `AREA`    | Area                 |
| `CITY`    | City                 |
| `REGION`  | Region, state        |
| `COUNTRY` | Country              |

Some property names are reserved for future use:

| Property  | Description                     |
|:----------|:--------------------------------|
| `FP`      | Fingerprint biometric data      |
| `VOICE`   | Voice biometric data            |
| `FR`      | Face recognition biometric data |

After all properties have been listed, the client signs the identity using a `clientSignature` element. Client signatures are calculated
as follows:

* The signature is calculated on the identity element excluding the `id` attribute and the `clientSignature`, `status` and `serverSignature`
elements.
* All text nodes and attribute values contain XML-encoded normalized Unicode text (in NFC).
* XML is normalized. Unnecessary white space removed. Space characters only allowed whitespace.
* The normalized XML, with attributes in alphabetical order, using double quotes, xmlns attributes only when required, 
`&`, `<`, `>`, `"` and `'` consistently escaped, empty elements are closed using />, and no space when ending empty element, 
is UTF-8 encoded before being signed.
* The identity element never includes the `xmlns` attribute when calculating the signature.
* If RSA is used to sign the identity, `s1` contains the signature, which must be calculated using SHA-256 and PSS.
* If ECC is used to sign the identity, ECDSA with SHA-256 is used. `s1` and `s2` attributes contain the first and second signature values
correspondingly.

**Note**: The purpose of the signature, is for the server to validate that the client has access to the private keys corresponding to the 
public keys registered with the trust provider, and that the contents of the identity is consistent over time.

**Note**: Legal identities are updated by the client regularly. Check with the server to get the most recent legal identity, if needed.

**Note**: Whitespace and indentation in the example above has been added for readability only.

**Note**: Legal identities are case insensitive in searches and references.

Example:

```xml
<iq type='set' id='14' to='legal.example.org'>
	<apply xmlns="urn:ieee:iot:leg:id:1.0">
		<identity>
			<clientPublicKey>
				<p521 
					x="AdHM7OPHVHBNJmaZ7VI33yCbHUcCIPD0cIDS26eKnSN14i2wVanxHrwshWCHk7CG2q1kSXGyXpKHojzrGWWNU3CG" 
					xmlns="urn:ieee:iot:e2e:1.0" 
					y="i90GqjaVZgYhd4aJyVKMmhW8Ma75CxeuQQUC+H2cHn5SJ8Kw0w9OPFEdgsTO/Q1LmrBfj+HLus4lgpZCQizWIGs="/>
			</clientPublicKey>
			<property name="FIRST" value="Jon"/>
			<property name="LAST" value="Doe"/>
			<property name="PNR" value="123456789"/>
			<property name="ADDR" value="Street Nr 1"/>
			<property name="ZIP" value="12345"/>
			<property name="AREA" value="Area"/>
			<property name="CITY" value="City"/>
			<property name="REGION" value="State"/>
			<property name="COUNTRY" value="Country"/>
			<clientSignature 
				s1="u5qgLeff84H6UinxLRHHmk/HSy2sJ29epIFY2nQ9jqb+mjVOhLsQkJjlhYitZecSw/i9wPqjxBGE9Z6ON8k8OoY=" 
				s2="Iu3vi7ld1BQT+GsmeUeaXxnZznEnMq1+UHAvLP89hhDt63doJ/f2BuOCp62u0LZCh5AXIjFPpwj5ks3m+lQPqwE="/>
		</identity>
	</apply>
</iq>
```

After passing all validation tests by the server, it responds with an annotated `identity` element back to the client. The server attaches
an reference identity to the legal identity, which it makes available in the `id` attribute of the `identity` element.
The identifier is formed as a JID, but is not a JID. The domain part corresponds to the domain of the Trust Provider.
The `identity` element provided by the server contains the original information provided by the client, as well as some state information 
about the identity, encoded in a `status` element. This element can have the following attributes:

| Attribute   | Type            | Use      | Description                                                                       |
|:------------|:----------------|:---------|-----------------------------------------------------------------------------------|
| `provider`  | `xs:string`     | Required | JID of Trust Provider validating the correctness of the identity.                 |
| `state`     | `IdentityState` | Required | Contains information about the current statue of the legal identity registration. |
| `created`   | `xs:dateTime`   | Required | When the legal identity was first created.                                        |
| `updated`   | `xs:dateTime`   | Optional | When the legal identity was last updated.                                         |
| `from`      | `xs:date`       | Optional | From what date (inclusive) the legal identity can be used.                        |
| `to`        | `xs:date`       | Optional | To what date (inclusive) the legal identity can be used.                          |

The `state` attribute can have one of the following values:

| IdentityState                                                                                           ||
|:--------------|:-----------------------------------------------------------------------------------------|
| `Created`     | An application has been received and is pending confirmation out-of-band.                |
| `Rejected`    | The legal identity has been rejected.                                                    |
| `Approved`    | The legal identity is authenticated and approved by the Trust Provider.                  |
| `Obsoleted`   | The legal identity has been explicitly obsoleted by its owner, or by the Trust Provider. |
| `Compromized` | The legal identity has been reported compromized by its owner, or by the Trust Provider. |

Finally, the server signs the identity to attest to the validity and integrity of the information encoded inside. This signature is
encoded in the `serverSignature` element. The server signature is calculated as follows:

* The signature is calculated on the `identity` element excluding the `serverSignature` element.
* All text nodes and attribute values contain XML-encoded normalized Unicode text (in NFC).
* XML is normalized. Unnecessary white space removed. Space characters only allowed whitespace.
* The normalized XML, with attributes in alphabetical order, using double quotes, xmlns attributes only when required, 
`&`, `<`, `>`, `"` and `'` consistently escaped, empty elements are closed using />, and no space when ending empty element, 
is UTF-8 encoded before being signed.
* The identity element never includes the `xmlns` attribute when calculating the signature.

**Note**: The purpose of the server signature, is to validate the legal identity to other clients that have access to the server public keys.

**Note**: Server keys may change over time. If a signature does not validate, make sure to get the most recent public key from the server 
and check signature again.

Example:

```xml
<iq id='14' type='result' to='client@example.org/990d3f8fbb7f5a0bf39c7fdeda1ac9d7' from='legal.example.org'>
	<identity id="d1058d7b-8693-4e26-a56b-45d3cb3c3f39@legal.example.org" xmlns="urn:ieee:iot:leg:id:1.0">
		<clientPublicKey>
			<p521 
				x="AdHM7OPHVHBNJmaZ7VI33yCbHUcCIPD0cIDS26eKnSN14i2wVanxHrwshWCHk7CG2q1kSXGyXpKHojzrGWWNU3CG" 
				xmlns="urn:ieee:iot:e2e:1.0" 
				y="i90GqjaVZgYhd4aJyVKMmhW8Ma75CxeuQQUC+H2cHn5SJ8Kw0w9OPFEdgsTO/Q1LmrBfj+HLus4lgpZCQizWIGs="/>
		</clientPublicKey>
		<property name="FIRST" value="Jon"/>
		<property name="LAST" value="Doe"/>
		<property name="PNR" value="123456789"/>
		<property name="ADDR" value="Street Nr 1"/>
		<property name="ZIP" value="12345"/>
		<property name="AREA" value="Area"/>
		<property name="CITY" value="City"/>
		<property name="REGION" value="State"/>
		<property name="COUNTRY" value="Country"/>
		<clientSignature 
			s1="u5qgLeff84H6UinxLRHHmk/HSy2sJ29epIFY2nQ9jqb+mjVOhLsQkJjlhYitZecSw/i9wPqjxBGE9Z6ON8k8OoY=" 
			s2="Iu3vi7ld1BQT+GsmeUeaXxnZznEnMq1+UHAvLP89hhDt63doJ/f2BuOCp62u0LZCh5AXIjFPpwj5ks3m+lQPqwE="/>
		<status 
			created="2018-12-03T09:07:11.903" 
			from="2018-12-03T00:00:00.000" 
			provider="legal.example.org" 
			state="Created" 
			to="2020-12-03T00:00:00.000"/>
		<serverSignature 
			s1="PTB/613CRlXXs95qH5xSfxFfo7rE5nEVHSo48sbHYnLpnRV9VA4mBl0CypH9vJewhmvyB84ypZQ/8IIBuuu3jMY=" 
			s2="iUMVrYVKBU2Qd/cjCsbzMVafGp0Gt9w//luOkC1N1IpndkBsU8PgLYctIIiePt7bGvIDtsoVlgzMUW44UpJesLE="/>
	</identity>
</iq>
```

Identity state changes
----------------------------

Whenever the state of the legal identity is changed on the server, a message is sent to the bare JID of the account containing the identity,
in an `identity` element. Identities must only be accepted, if the provider corresponds to the sender, and if the server signature is 
valid, and corresponds to the public key of the server.

Example:

```xml
<message to='client@example.org/5d4200a3398a75a078c2cbace1668f67' from='legal.example.org'>
	<identity id="d1058d7b-8693-4e26-a56b-45d3cb3c3f39@legal.example.org" xmlns="urn:ieee:iot:leg:id:1.0">
		<clientPublicKey>
			<p521 
				x="AdHM7OPHVHBNJmaZ7VI33yCbHUcCIPD0cIDS26eKnSN14i2wVanxHrwshWCHk7CG2q1kSXGyXpKHojzrGWWNU3CG" 
				xmlns="urn:ieee:iot:e2e:1.0" 
				y="i90GqjaVZgYhd4aJyVKMmhW8Ma75CxeuQQUC+H2cHn5SJ8Kw0w9OPFEdgsTO/Q1LmrBfj+HLus4lgpZCQizWIGs="/>
		</clientPublicKey>
		<property name="FIRST" value="Jon"/>
		<property name="LAST" value="Doe"/>
		<property name="PNR" value="123456789"/>
		<property name="ADDR" value="Street Nr 1"/>
		<property name="ZIP" value="12345"/>
		<property name="AREA" value="Area"/>
		<property name="CITY" value="City"/>
		<property name="REGION" value="State"/>
		<property name="COUNTRY" value="Country"/>
		<clientSignature 
			s1="u5qgLeff84H6UinxLRHHmk/HSy2sJ29epIFY2nQ9jqb+mjVOhLsQkJjlhYitZecSw/i9wPqjxBGE9Z6ON8k8OoY=" 
			s2="Iu3vi7ld1BQT+GsmeUeaXxnZznEnMq1+UHAvLP89hhDt63doJ/f2BuOCp62u0LZCh5AXIjFPpwj5ks3m+lQPqwE="/>
		<status 
			created="2018-12-03T09:07:11.903" 
			from="2018-12-03T00:00:00.000" 
			provider="legal.example.org" 
			state="Approved" 
			to="2020-12-03T00:00:00.000" 
			updated="2018-12-03T13:43:52.357"/>
		<serverSignature 
			s1="AQxRSvNkmLFPiaVjD/yK8zOEDqlEH/JHznfG7e8aqFgEWTr9CRIJig/OD15+2G0jSrq1rJ0stUif+QfnFWOmygO2" 
			s2="rZy2+c5sJ97teactS02kr25nggOWxwi7sCBWpiARG08FZt4yh4kmilPIvt5a/TezQiqDziFr/4e0OerKBZcllws="/>
	</identity>
</message>
```

Getting legal identity
---------------------------

You can get a legal identity from the server, if it belongs to you. You send the `getLegalIdentity` element 
with the `id` attribute set to the identity of the legal identity object in an `iq get` to the server.

**Note**:

* To get the legal identity from a signature, see `validateSignature`.
* To get the legal identities related to contracts, see `getLegalIdentities` element in the 
[Smart Contracts](/SmartContracts.md) namespace.</xs:documentation>

Example:

```xml
<iq type='get' id='5' to='legal.example.com'>
	<getLegalIdentity 
		id="2397feaa-b09b-5cd2-2035-7fd4dffe66da@legal.example.com"
		xmlns="urn:ieee:iot:leg:id:1.0"/>
</iq>
```

The server responds, after making sure you're authorized to view the identity, with 
an `identity` object representing the legal identity you requested for.

Example:

```xml
<iq id='5' type='result' to='test@example.com/3f9fafc29d7c1556479236b0424cae0b' from='legal.example.com'>
	<identity 
		id="2397feaa-b09b-5cd2-2035-7fd4dffe66da@legal.example.com" 
		xmlns="urn:ieee:iot:leg:id:1.0">
		<clientPublicKey><p521 
			x="AUVVaK2CqS0xFIrnPXIFhqtsjX2z3k78NzpkfR9lcariRrU4uKSclF/hHRQnWOrsycS7Gli1wxeCU1peby2siKu8" 
			xmlns="urn:ieee:iot:e2e:1.0" 
			y="AUvIf2MNFo/D13zzjtcnV/cuPbwrKYtagEOBQdeLn9nt4l5KjgsOfRVz3HYhQaVkNCsfPBfOukiyhwF3Zm25MtmX"/>
		</clientPublicKey>
		<property name="FIRST" value="Jon"/>
		<property name="LAST" value="Doe"/>
		<property name="PNR" value="123456789-0"/>
		<property name="ADDR" value="Street 1A"/>
		<property name="ZIP" value="12345"/>
		<property name="CITY" value="Metropolis"/>
		<clientSignature 
			s1="AUh1nusYAdvHjqoc07qS4i5vNtIlE74S2rqh8Wa0VmXtWPOp8cQEGPvWMKU/uio382kHtxPC75+BQd/GVyS7eE/R" 
			s2="Ad7xqwmXnqj99X6uAAVnn7vOpubYx+VB/hC5Z4Eku50+ryOt4p9IKZu9NDyddc3yUUGfbAANYxJXFnTcY92s8hxS"/>
		<status 
			created="2018-12-03T15:48:09.113" 
			from="2018-12-03T00:00:00.000" 
			provider="legal.example.com" 
			state="Created" 
			to="2020-12-03T00:00:00.000"/>
		<serverSignature 
			s1="Ad4zbcpg4hfjVjrVx10KVODtfXg4ChzBOEtmvVNzl+8Ndic4CurCYNmGrgO+weIVm6IdOY7nE2MLOa9EpjVzxvoT" 
			s2="Ae1GHPA2uct5UZLBQBlao4wvGL7ZZKbv9Q4V0X21XtUQMVCXGesrQow+46ktWo59OjN3XSnaF1i+6u3qlmJ96M7w"/>
	</identity>
</iq>
```

Validating signature
---------------------------

If an endpoint receives a signature on some data, referened only through its legal identity ID, the endpoint can 
ask the Trust Provider hosting the legal identity to validate the signature. If it is valid, the Trust Provider 
returns the legal identity.

Example:

```xml
<iq type='get' id='5' to='legal.example.com'>
	<validateSignature 
		id="239ea9bd-eb6f-8cd2-d827-82b0cbd8df6f@legal.example.com" 
		data="Ec5lgmA27Rw2nsYzLFl5FhG4hL7QOlqgMLVaU5GjwmmylZhENzdA...YGDyXwP+dApo0YpOxWwQl0EAkc1CQ==" 
		s1="2EaIOdKnCNV/IMHOIwIJN+4npf98kFF3OmZmxpNMjKXaQn/8xg+9RPuOis3E06i6ozlFZ0oReN6Cx8LONPGDJFo=" 
		s2="a7Bv3IrRcKw4o3JUGzjf3Vj8fA+x/dFLEJSuFZc8fZjJZBuFTEEjMVQc0zlMx++TqEo5//vED6WBmQdLKcXB1qA=" 
		xmlns="urn:ieee:iot:leg:id:1.0"/>
</iq>
```

If the server finds the signature match the public key of the legal identity, it returns information
about the legal identity in an `identity` element.

**Note**: If omitting the `id` attribute, current approved legal identities will be used to validate
the signature.

Example:

```xml
<iq id='5' type='result' to='test@example.com/74078396758ac70c977329f57e078c8a' from='legal.example.com'>
	<identity id="239ea9bd-eb6f-8cd2-d827-82b0cbd8df6f@legal.example.com" xmlns="urn:ieee:iot:leg:id:1.0">
		<clientPublicKey>
			<p521 
				x="AUVVaK2CqS0xFIrnPXIFhqtsjX2z3k78NzpkfR9lcariRrU4uKSclF/hHRQnWOrsycS7Gli1wxeCU1peby2siKu8"
				xmlns="urn:ieee:iot:e2e:1.0"
				y="AUvIf2MNFo/D13zzjtcnV/cuPbwrKYtagEOBQdeLn9nt4l5KjgsOfRVz3HYhQaVkNCsfPBfOukiyhwF3Zm25MtmX"/>
		</clientPublicKey>
		<property name="FIRST" value="Jon"/>
		<property name="LAST" value="Doe"/>
		<property name="PNR" value="123456789-0"/>
		<property name="ADDR" value="Street 1A"/>
		<property name="ZIP" value="12345"/>
		<property name="CITY" value="Metropolis"/>
		<clientSignature
			s1="AYxs3/nWkVUzRnrUV6w3K0iJP+zg0CVaeP+d2vaQBwHrEECE3DoIfwN/fXDuUIPCyw5m3a5VvLxCOchxpHhWXHYh"
			s2="CvRjXPABEcbgNB4eJt3QPemochpSPmg1IiWMI4RpWt6nK3VM2+yhxVBrpUEMiBX9lqQAuxDHw1njOkQkmN/HA7M="/>
		<status
			created="2018-12-08T17:11:41.678"
			from="2018-12-08T00:00:00.000"
			provider="legal.example.com"
			state="Created"
			to="2020-12-08T00:00:00.000"/>
		<serverSignature
			s1="AdoTZ/oUUb559L/KjKygS/y6daseHPy+cBF9DtyDqf8xNEzo8Sq4iay6AseYDZl7hW3858JLgapJ3oqr6fbqL9rE"
			s2="Ab38iluaAk1f2GDJD+/67qmN7xhItIdy/PcMDqM57Ps7e0UTct92aWGuX86xG195Qy4PsrzOS9sGwCpA1LEFNLeM"/>
	</identity>
</iq>
```

Obsoleting legal identity
---------------------------

A client can obsolete one of its legal identities on the server. The client sends the 
`obsoleteLegalIdentity` element `with the `id` attribute set to the identity of the 
legal identity object in an `iq set` to the server.

Example:

```xml
<iq type='set' id='5' to='legal.example.com'>
	<obsoleteLegalIdentity 
		id="23981af6-cae1-0b55-4c58-64d023fe4dcb@legal.example.com" 
		xmlns="urn:ieee:iot:leg:id:1.0"/>
</iq>
```

The server responds, after making sure you're authorized to update the identity, with 
an `identity` object representing the updated legal identity.

Notes:

* Obsoleting an legal identity application (in `Created` state) automatically turns it 
to `Rejected`.
* Trying to obsolete a rejected or compromized identity returns a forbidden error.

Example:

```xml
<iq id='5' type='result' to='test@example.com/6315e90c2c092d4321ef2ac27d9258bc' from='legal.example.com'>
	<identity id="23981af6-cae1-0b55-4c58-64d023fe4dcb@legal.example.com" xmlns="urn:ieee:iot:leg:id:1.0">
		<clientPublicKey>
			<p521 
				x="AUVVaK2CqS0xFIrnPXIFhqtsjX2z3k78NzpkfR9lcariRrU4uKSclF/hHRQnWOrsycS7Gli1wxeCU1peby2siKu8" 
				xmlns="urn:ieee:iot:e2e:1.0" 
				y="AUvIf2MNFo/D13zzjtcnV/cuPbwrKYtagEOBQdeLn9nt4l5KjgsOfRVz3HYhQaVkNCsfPBfOukiyhwF3Zm25MtmX"/>
		</clientPublicKey>
		<property name="FIRST" value="Jon"/>
		<property name="LAST" value="Doe"/>
		<property name="PNR" value="123456789-0"/>
		<property name="ADDR" value="Street 1A"/>
		<property name="ZIP" value="12345"/>
		<property name="CITY" value="Metropolis"/>
		<clientSignature 
			s1="AXU0OvfbgvWsh+Qa1JYZSVXwYBknmmJN/zMpuemxoeLAPn6proshoQwo5JBS9MYhpXcVl8xnxKUbH2NUFU7jUIFw" 
			s2="AQfBtPAt/7vNR7a9GuMDFdqIRDgbC8Km2zp5B/oOuE8+3GOB5oVh2mgQ6sM4XUO2KYhkXtIDwHHlP1bdlgfbZSRE"/>
		<status 
			created="2018-12-03T17:48:54.770" 
			from="2018-12-03T00:00:00.000" 
			provider="legal.example.com" 
			state="Obsoleted" 
			to="2020-12-03T00:00:00.000" 
			updated="2018-12-03T17:48:55.301"/>
		<serverSignature 
			s1="n0nTiNjTLNjf4K/iav7qEWeDCVuSVqOLjJoW5GK3AP2TxyLVF9IaQRCDggsMiinrD5aFsMpJeuaL79eLmopz/bw=" 
			s2="AVYwbzheFDdibf306dePgIDDnr2ekvLvfmycZue4DXFRX6D8/hQD/KHgW8iAj2mLMdBP3Yq8Txe52telc859PwRZ"/>
	</identity>
</iq>
```

Reporting a legal identity as compromized
---------------------------------------------

A client can report one of its legal identities as compromized on the server. The client 
sends the `compromizedLegalIdentity` element `with the `id` attribute set to the identity of the 
legal identity object in an `iq set` to the server.

Example:

```xml
<iq type='set' id='5' to='legal.example.com'>
	<compromizedLegalIdentity 
		id="23981ed9-0580-fdb6-3c92-4a58fda89b26@legal.example.com" 
		xmlns="urn:ieee:iot:leg:id:1.0"/>
</iq>
```

The server responds, after making sure you're authorized to update the identity, with 
an `identity` object representing the updated legal identity.

Notes:

* Reporting a legal identity application (in `Created` state) as compromized 
automatically turns it to `Rejected`.
* Trying to report a rejected identity as compromized returns a forbidden error.

Example:

```xml
<iq id='5' type='result' to='test@example.com/ccb53e69cd36dec114edb1fa408d460a' from='legal.example.com'>
	<identity id="23981ed9-0580-fdb6-3c92-4a58fda89b26@legal.example.com" xmlns="urn:ieee:iot:leg:id:1.0">
		<clientPublicKey>
			<p521 
				x="AUVVaK2CqS0xFIrnPXIFhqtsjX2z3k78NzpkfR9lcariRrU4uKSclF/hHRQnWOrsycS7Gli1wxeCU1peby2siKu8" 
				xmlns="urn:ieee:iot:e2e:1.0" 
				y="AUvIf2MNFo/D13zzjtcnV/cuPbwrKYtagEOBQdeLn9nt4l5KjgsOfRVz3HYhQaVkNCsfPBfOukiyhwF3Zm25MtmX"/>
		</clientPublicKey>
		<property name="FIRST" value="Jon"/>
		<property name="LAST" value="Doe"/>
		<property name="PNR" value="123456789-0"/>
		<property name="ADDR" value="Street 1A"/>
		<property name="ZIP" value="12345"/>
		<property name="CITY" value="Metropolis"/>
		<clientSignature 
			s1="XPXw7gzgXd+1xWahrHQ9OnCZ2n0H9PYPQDO79f3t9QHtOruBhN5X1vOyNrcNd5qWtSGI56qK7hJvQf17XdCDx8Y=" 
			s2="giOG6fzydpB4OEHgbIZ9wjjEtlVETOH7VQ3Y4Sfp9kNnyEC2Wt6y5iLi+SpEXCJl5UGQcY6qwWwQ1lQMaoxnci0="/>
		<status 
			created="2018-12-03T18:05:29.372" 
			from="2018-12-03T00:00:00.000" 
			provider="legal.example.com" 
			state="Compromized" 
			to="2020-12-03T00:00:00.000" 
			updated="2018-12-03T18:05:29.768"/>
		<serverSignature 
			s1="NUEAAIC+OltSTgjGaAMCF3hdEFUabp//p+uLw0nIonP3KkGAlBLG3dnV+vVdKrLsfhfv6SyaYdSwZmVcwF7IN5I=" 
			s2="AeDmcqUEJbUEPpN3qZsOVX3PN0afkKThitPhX+KMhJxjC6zNjCDo5dTH2nsj9OwhHCtFjeasowZpHni5deSLKo0h"/>
	</identity>
</iq>
```

Security considerations
------------------------------

### Client key compromized

In case the private key of the client is compromized, other entities will be able to sign using this key, and thus be able to create
fraudulent signatures in smart contracts. Since all signatures are attested by the server, these fraudulent signatures will be detected
if sent directly to other peers. To bypass this, an attacker would have to trick the server into attesting the signature. To do this,
the attacker would have to have access to the XMPP credentials also, to be able to connect as the client, and thus submit the fraudulent
signature using the correct account to the server. The server and peers subscribing to the presence of the client can detect this, since
an additional resource will be generated, and sent to presence subscribers.

To minimize the risk of attackers getting hold of both the private key, and the corresponding XMPP credentials of a client, these should
be stored in a protected storage, such as an encrypted database or key vault.

When suspecting a key might have been compromized, the compromized legal identity should be obsoleted by the client as soon as possible,
and a new key should be generated and an new legal identity applied for. There is no need to update contracts, since these are validated
by the server signature, and parts can retrieve the legal identity history, with timestamps and states, of all parts in the contract.

Clients are also encouraged to regularly create new keys and corresponding legal identities. Servers can enforce this by assigning a
limited timestamp for an identity when approviding it.

Clients should use keys with a security strength comparable to the server key security strength, with at least a minimum of 128, but
preferrably greater, depending on use cases involved.

### Server key compromized

In case the private key of te server is compromized, other entities will be able to create fraudulent smart contracts in the name of the
server. Clients who are parts in a contract can always retrieve the contract from the server using the contract identity. Doing this allows
clients to compare the server contract with the fraudulent contract, and detect differences, or if the contract at all exists. An attacker
would have to have control of the server, to be able to introduce fraudulent contracts into the system.

To minimize the risk of attackers getting hold of both the server private key, as be able to inject smart contracts, these should
be stored in a protected storage, such as an encrypted database or key vault.

If the server generates a new private key, any server signatures in attested artefacts have to be recaulcated to match the new public key.

The server should use a relatively high security strength for its keys, at least 192 or 256, depending on use case.

### Management of client signatures

Having access to a client signature, as well as the data on which the signature is calculated, provides the holder with the means
to access the information encoded in the legal identity as well. Therefore, client signatures should be handled as confidential,
by any entity who has been entrusted with the signatures.

Examples of where client signatures are used:

* In the encoding of the legal identities themselves. Having access to the legal identity, obviously already gives access to the same
legal identity. Care should be taken to manage the legal identities of others, since it is sensitive personal information.

* In signatures of smart contracts. Anyone with access to a signed smart contract, also have access to the contents and digital signatures
made by the legal identities signing the contracts. This gives the holder of the smart contract access to the legal identities of the
clients that have signed the contract, just as in the case of a normal contract. For this reason, care should de taken when managing 
smart contracts, and only give access to them to parties who are entrusted with managing the legal identities of the parts. Since the legal
identities are sensitive personal information, so are signed smart contracts.

* When a client signs something, and presents the signature to a third party. By signing something, gives the receiver of the signature
access to the information in the legal identity.

Note: Having access to the information encoded with the legal identity does not give the holder the ability to forge signatures using the
legal identity. To sign something using the legal identity, access to the private keys is required. Private keys are not encoded with the
legal identity, or stored on the Trust Provider, or evenr presented to the Trust Provider. Only the client itself should have access to its
private keys.
