End-to-End encryption
===============================

This document outlines the XML representation of end-to-end encryption, as defined by the IEEE XMPP IoT Working Group. The XML representation is modelled 
using an annotated XML Schema:

| End-to-End encryption                   ||
| ------------|----------------------------|
| Namespace:  | urn:ieee:iot:e2e:1.0       |
| Schema:     | [E2E.xsd](Schemas/E2E.xsd) |


Motivation and design goal
----------------------------

The method of end-to-end encryption (E2E) described here, is designed with the following goals in mind:

* Peers should be able to choose which algorithms to use.

* Hybrid algorithms should be supported, where an asymmetric cipher with a public/private key pair is used to negotiate a symmetric key for a symmetric
cipher, that is used to encrypt/decypt the payload of the stanza. The symmetric key is unique for each stanza.

* Both `message` and `iq` stanzas should be possible to encrypt, together with all their contents.

* Signatures should be used to authenticate sender.

* E2E communication is optional, and can be used where privacy or confidentiality concerns are such that E2E connections are warranted.

* Public keys should be short-lived to maintain forward secrecy. If a public/private key is broken, only stanzas encrypted with the help of that
public/private key pair are affected.


Publishing E2E Information
-------------------------------

When a device starts, it should generate new public/private keys. Everytime it gets connected it should publish the public key(s) using `presence`, so that
everyone with a presence subscription is aware of the currernt public key(s). Everytime new public/private keys are generated, the public keys should also be 
published in a new `presence`. Apart from the most recent public/private key pair, the previous pair should also be kept in memory. Since there might be a delay
in propagating the new key, it allows the recipient to receive and decrypt stanzas that have been encrypted using the previous key.

Hybrid Ciphers
--------------------

Cyphers used in E2E have two components: An assymetric cipher using a public key for encryption and signature validation, and a private key for
decryption and signature generation. This cipher is used to negotiate symmetric keys for an underlying symmetric cipher used to encrypt and decrypt the
actual payload. Once the symmetric key has been used, it can be discarded. Assymetric keys are regenerated as often as required by the sensitivity of
the data being communicated.

Presence instead of handshake
----------------------------------

To avoid having to negotiate keys when setting up E2E communication, public keys are published using `presence`. This allows anyone to send an end-to-end
encrypted message to the entity, without first having to do a handshake. The sender can make the decision without a handshake, and the receiver will be able 
to decrypt it. The number of E2E stanzas transmitted and received is the same as the number of unencrypted stanzas, plus the number of presence stanzas 
transmitted. While this typically reduces the number of messages required to estblish E2E communication between peers, it is not designed for the generation 
of new public keys for every single stanza. Instead, public/private keys are generated on a timely fashion, depending on context, or after concluding a series 
of stanzas (a communication session), so that anyone starting a new E2E session with the entity does so using new keys. Public keys should still be short-lived,
as to not risk all historic messages to be broken, in case a public/private key pair is broken (forward secrecy).

AES-256 based hybrid ciphers
---------------------------------

The AES-256 based hybrid ciphers presented here, use the AES-256 symmetric cipher to encrypt and decrypt content, while an asymmetric cipher provides the
AES key to use, as well as the content signature.

The Initiation Vector (IV) is calculated as follows, if nothing else is specified: It consists of the first 16 bytes of the 
SHA-256 hash of the UTF-8 encoded concatenation of the `id`, `type`, `from` and `to` attributes of the stanza element, in that order.

The data to encrypt is prefixed by its length. The number of bytes used for the length is variable. The length is encoded as a sequence of 7-bit value bytes
(least significant part first). The 8th bit is used to inform the reader if more length bytes are following (1), or if the byte is the last length byte (0).
Following the encoded data length, follows the data to be encrypted. AES-256 has a block size of 16 bytes. Any unused bytes in the last block are filled
with random bytes before encryption. Blocks are chained together during encryption using Cipher Block Chaining (CBC).

When E2E encrypting a `message` stanza, it is encrypted in its entirety (entire XML stanza), and then placed in an `aes` element. This is then sent in a 
**normal**, unadorned `message` stanza by itself. The only attributes transferred from the original message, are the `id` and `to` attributes. The rest is 
protected inside the encrypted element.

When E2E encrypting an `iq` stanza, only the contents of the stanza is encrypted, and then placed in an `aes` element. This element is then sent in an
`iq` stanza with the same `type`, `id`, `to` and `from` attributes as the original stanza.

In both cases, the signatures are calculated on the unencrypted part of the payload that is to be encrypted. Encoding of XML text to bytes is always done
using UTF-8 encoding.

### RSA/AES

The `rsa` algorithm combines RSA public/private keys with AES-256 symmetric cipher. The RSA keys are used to encrypt all AES keys used
(using SHA-256 and OAEP padding), as well as to sign the payload by the sender (using SHA-256 and PSS padding).

Support for RSA/AES encryption by an endpoint is shown by including the `rsa` element inside the `e2e` element in the `presence` stanza. To alert the
recipient that RSA/AES is used, the `keyRsa` and `signRsa` attributes are used by the `aes` element. `keyRsa` contains the Base-64 encoded RSA-encrypted
AES key to use to decrypt the payload. The `signRsa` attribute contains the signature of the decrypted payload, made by the sender.

### ECC/AES

Elliptic Curve Cryptography can be used together with the AES-256 symmetic cipher to encrypt content between endpoints. The specific Elliptic Curve is used
to derive a common key for AES (using ECDH), as well as to sign the unencrypted content (using ECDSA).

Support for EEC/AES encryption by an endpoint is shown by including the curve algorithm element inside the `e2e` element in the `presence` stanza. Each curve
element include `x` and `y` attributes containing the coordinates of the corresponding public key (most significant byte first, and then Base-64 encoded). 
To alert the recipient that EEC/AES is used, the `ec`, `ecdsa1` and `ecdsa2` attributes are used by the `aes` element. `ec` contains the name of the curve 
being used, while `ecdsa1` and `ecdsa2` contains the two signature integers generated by ECDSA (most significant byte first, and then Base-64 encoded).

| Curve Name | Element   | Security level | RSA equivalent |
|:-----------|:----------|:--------------:|:--------------:|
| NIST P-192 | `p192`    | 96             | 1024           |
| NIST P-224 | `p224`    | 112            | 2048           |
| NIST P-256 | `p256`    | 128            | 3072           |
| NIST P-384 | `p384`    | 192            | 7680           |
| NIST P-521 | `p521`    | 256            | 15360          |

Examples
-----------

Example, when publishing public keys using `presence`:

```
<presence>
   <show>chat</show>
   <e2e xmlns='urn:ieee:iot:e2e:1.0'>
      <p521 x='gInX2HA3BD5fCyxDoqt6FFzKU...' y='9hAm0F4/4tmAKvkweQnMsk...'/>
      <p384 x='ccw28YXo/VWYOAFTnNKkqOToN...' y='PazgeIFpJhdjRoHd0ARQSI...'/>
      <p256 x='Tmz/J8/k2x8wYFm3hpqwGm17W...' y='8o9a4JpBs2KOki657Rp+qd...'/>
      <p224 x='33lAKAeM77sGOOC0pTfVmMeMl...' y='E7bNDNZ4kxzBx54Ky3xxDd...'/>
      <p192 x='ufDM/23BvMjDKFdkCAdd03jrj...' y='FFFBPlrFv1Y8JYVd0fVhJ8...'/>
      <rsa size='4096' mod='vyhg+bU+QqExe7QRpKWXiRG/LkOU6D/...' exp='AQAB'/>
   </e2e>
   <p2p extIp='93.184...' extPort='49790' locIp='192.168.0.102' locPort='49790' xmlns='urn:ieee:iot:p2p:1.0'/>
   <c xmlns='http://jabber.org/protocol/caps' hash='sha-256' node='...' ver='...'/>
</presence> 
```

Example, sending an E2E encrypted `iq` stanza using RSA/AES:

```
<iq id='28209' type='get' to='...' from='...'>
   <aes xmlns='urn:ieee:iot:e2e:1.0'
        keyRsa='G1NwbnHifFUhYWIrBc3K...' 
        signRsa='mX1DQF/HKzPTfQzTAWF...'>
      bpg/e2lnVqiT8IE0KCH7l...
   </aes>
</iq> 
```

**Note**: RSA keys are large, which will cause small stanzas to become much larger when E2E encrypted. RSA and AES is available for devices that do not
support Elliptic Curve Cryptography (ECC). ECC is able to maintain the same security strength using shorter messages.

Example, sending an E2E encrypted `iq` stanza using EEC/AES:

```
<iq id='304' type='get' to='...' from='...'>
   <aes xmlns='urn:ieee:iot:e2e:1.0'
        ec='NIST P-256'
        ecdsa1='KILmz4GOURVGyRxnuPpqamDSC2zhvuHaxyvjLxonGD4='
        ecdsa2='+A5RuHBq2pUfk4i+D52D8MouNd36isiOcGz1rKdpHgk='>
      COS0RYr53vz9C+FoKbGgeVUewBDPvP5QESsiUbBQKrUpwv8rXTUrdKbsomQgW5oH
   </aes>
</iq>
```