Smart Contracts
=====================

Users with legal identities can sign smart contracts. Smart contracts are machine-readable contracts that are legally binding for the
parts that have signed them. Smart contracts are divided into two parts: One container, that encapsulates the contract and provides state
information and signatures. The second part is an XML document, whose semantic meaning is defined by the qualified name of the root.
By providing XML schemans, the server can make sure contracts are well-defined and contain all required information. The server attests
to the validity of the contents of the contract, its integrity and all signatures. Contracts can be used to automate different aspects
in a smart city, such as provisioning for instance.

| Legal Identities                                                      ||
| ------------|----------------------------------------------------------|
| Namespace:  | urn:ieee:iot:leg:sc:1.0                                  |
| Schema:     | [SmartContracts.xsd](Schemas/SmartContracts.xsd)         |
