Transport Encryption
========================

XMPP supports **ubiquitous encryption**. This means all connections and all transports are encrypted. The methods used to encrypt each transport depends
on the underlying binding method used:

* For traditional socket connections, HTTP-based connections or websocket connections, TLS transport encryption is used. Brokers are authenticated using
certificates, and clients using SASL.

* For experimental UDP connections, DTLS is used.

* In federated communications, server connections are encrypted using TLS. Domains are validated using certificates and dial-back
[XEP-0220](https://xmpp.org/extensions/xep-0220.html).

Note however, that transport encryption only protect against eavesdropping. Encrypted stanzas are decrypted at each node. To make sure the contents
of stanzas are not compromized at the nodes, use [End-to-End encryption](E2eEncryption.md).