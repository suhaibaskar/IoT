Federation
========================

Federation is the process in which brokers cooperate on the Internet to pass stanzas between different domains. When an entity sends a stanza to an
entity on another domain, the first broker connects to the second (unless a connection already exists), and establishes a connection between the two.
The second broker connects to the first just to make sure the first is the broker it claims to be. In both connections, the certificate of the other is
used to validate the domain name of the corresponding broker. If both brokers have been successfully validated, the first broker sends the stanza to the
second, who in turn forwards it to the corresponding client. This process of federation allows XMPP networks go grow globally in a natural way, without
limiting participants with whom they are able to communicate.

![Federation](Diagrams/Federation.png)
